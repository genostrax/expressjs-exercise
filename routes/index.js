var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Operation On Two Numbers' });
});

router.post('/submit-data', (req, res) => {

  if(req.body.op === "add") {
		const sum = parseInt(req.body.numberOne) + parseInt(req.body.numberTwo);
		res.render('index', { 
			title: 'Operation On Two Numbers',
		  result: req.body.numberOne+" + "+req.body.numberTwo+" = "+sum 
		});
  } 
  
  else if(req.body.op === "subtract") {
    const sub = parseInt(req.body.numberOne) - parseInt(req.body.numberTwo);
		res.render('index', { 
			title: 'Operation On Two Numbers',
		  result: req.body.numberOne+" - "+req.body.numberTwo+" = "+sub 
		});
  }
  
  else if(req.body.op === "multiply") {
    const mul = parseInt(req.body.numberOne) * parseInt(req.body.numberTwo);
		res.render('index', { 
			title: 'Operation On Two Numbers',
		  result: req.body.numberOne+" x "+req.body.numberTwo+" = "+mul 
		});
  }
  
  else if(req.body.op === "divide") {
    const div = parseInt(req.body.numberOne) / parseInt(req.body.numberTwo);
    if(parseInt(req.body.numberOne) === 0 || parseInt(req.body.numberTwo) === 0) { 
			res.render('index', { 
				title: 'Operation On Two Numbers',
				result: 'Division by 0 not allowed'
			});
		}
		else {
		  res.render('index', { 
				title: 'Operation On Two Numbers',
				result: req.body.numberOne+" / "+req.body.numberTwo+" = "+div 
			});
		}
  }
});

module.exports = router;
